#!/bin/bash

#Programa: Backup de usuario
#Nome: fastbkp:v0.1
#Autor: Danilo Luis Bernardo
#Data: 31-Jan-2019

#Realiza o backup dos arquivos de usuario

#Variáveis
PASTA_USUARIO=$(users)
PASTA_BKP=$(cd ~/Documentos/Backup)
OPCOES_BKP=`ls /home/${PASTA_USUARIO}`
DATA=$(date '+%d%m%Y')

#Funções
criar_pasta_bkp(){
    if [ ! -d ~/Documentos/Backup ];
    then
        mkdir ~/Documentos/Backup
    fi
}

echo " _____         _   _     _          " 
echo "|  ___|_ _ ___| |_| |__ | | ___ __  " 
echo "| |_ / _  / __| __| '_ \| |/ / '_ \ " 
echo "|  _| (_| \__ \ |_| |_) |   <| |_) |"
echo "|_|  \__,_|___/\__|_.__/|_|\_\ .__/ "
echo "                             |_|    "
echo ""

# formato_de_compactacao(){
#     echo "Escolha o formato de compactação"
#     echo "1 - ZIP"
#     echo "2 - TAR"
#     echo "3 - TAR.GZ"
#     echo "4 - TAR.BZ2"
#     read FORMATO_BKP

#     case ${FORMATO_BKP} in
#         1) if [ ${OPCAO} -eq 1 ];
#             then
#                 zip -r ~/Documentos/Backup/backup_${PASTA_USUARIO}.zip /home/${PASTA_USUARIO}
#             else
#                 zip -r ~/Documentos/Backup/backup_${PASTA_USUARIO}.zip /home/${PASTA_USUARIO}/${CAMINHO_ESPECIFICO}
#             fi ;;

#         2) if [ ${OPCAO} -eq 1 ];
#             then
#                 tar -zcf ~/Documentos/Backup/backup_${PASTA_USUARIO}.tar /home/${PASTA_USUARIO}
#             else
#                 tar -zcf ~/Documentos/Backup/backup_${PASTA_USUARIO}.zip /home/${PASTA_USUARIO}/${CAMINHO_ESPECIFICO}
#             fi ;;

#         3) if [ ${OPCAO} -eq 1 ];
#             then
#                 tar -cf ~/Documentos/Backup/backup_${PASTA_USUARIO}..tar /home/${PASTA_USUARIO}
#             else
#                 tar -cf ~/Documentos/Backup/backup_${PASTA_USUARIO}.zip /home/${PASTA_USUARIO}/${CAMINHO_ESPECIFICO}
#             fi ;;

#         *) echo "Opção inválida" ;;
#     esac
# }

echo "Deseja realizar o backup da pasta /home/${PASTA_USUARIO}? (S/n)"
read CONFIRMACAO

if [ ${CONFIRMACAO} == "n" ];
then
    echo "Backup cancelado"
    exit 0
elif [ ${CONFIRMACAO} == "S" ]
then
    while [ "${OPCAO}" != "1" -o "${OPCAO}" != "2" ];
    do
        echo "1 - Realizar backup completo"
        echo "2 - Realizar backup de pasta específica"
        echo "0 - Cancelar"
        read OPCAO

        case ${OPCAO} in
            1) criar_pasta_bkp
                tar -zcf ~/Documentos/Backup/bkp_${PASTA_USUARIO}_${DATA}.tar /home/${PASTA_USUARIO}
                exit 0 ;;

            2) criar_pasta_bkp
                echo "Pastas disponíveis:"
                echo "${OPCOES_BKP}"
                echo "Digite o restante do caminho /home/${PASTA_USUARIO}/.."
                read CAMINHO_ESPECIFICO
                tar -zcf ~/Documentos/Backup/bkp_${PASTA_USUARIO}_${CAMINHO_ESPECIFICO}_${DATA}.zip /home/${PASTA_USUARIO}/${CAMINHO_ESPECIFICO}
                exit 0 ;;

            0) echo "Cancelando backup"
                exit 0 ;;
        
            *) echo "Opção inválida!" ;;
        esac    
    done
else
    echo "Opção invalida, encerrando!"
fi